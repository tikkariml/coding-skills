#include<iostream>

int main(){
    bool green_light_on = {false};
    bool red_light_on = {true};

    if (green_light_on) {
        std::cout<< "light is green" <<std::endl;
    } else {
        std::cout<< "green light is off" <<std::endl;
    }

    if (red_light_on) {
        std::cout<< "light is red" <<std::endl;
    } else {
        std::cout<< "red light is off" <<std::endl;
    }
    return 0;
}