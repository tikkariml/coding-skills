#include<iostream>

int main(){
    int value {45};
    std::cout << "The value is: " << value << std::endl;

    value += 5; // value = 45 + 5
    std::cout << "The value is (after +=5): " << value << std::endl; // 50

    value -= 5; // value = 50 - 5
    std::cout << "The value is (after -=5): " << value << std::endl; // 45

    value *= 2; // value = 45 * 2
    std::cout << "The value is (after *=2) : " << value << std::endl; // 90

    value /= 3; // value = 90 / 3
    std::cout << "The value is (after /=3) : " << value << std::endl; // 30

    value %= 11; // vaöue 30 % 11
    std::cout << "The value is (after %=11) : " << value << std::endl; // 8

    return 0;
}