#include<iostream>

int main(){
    auto var1 {12}; // int
    auto var2 {13.0};  //double
    auto var3 {14.0f};  //float
    auto var4 {15.0l};  //long double
    auto var5 {'e'};  //char

    return 0;
}