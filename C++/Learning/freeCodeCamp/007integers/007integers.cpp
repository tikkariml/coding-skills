#include<iostream>
#include<string>

int main(){

    //Braced initializers
    //Variable may contain random garbage value . WARNING
    int elephant_count;

    //Initializer to zero
    int lion_count{};

    //Initializer to 10
    int dog_count{10};

    //Initializer to 15
    int cat_count{15};

    //Can use expression as initualizer
    int domesticated_animals{ dog_count + cat_count };

    int narrowing_conversion{2};

    std::cout<< "elephant count: "<< elephant_count <<std::endl;
    std::cout<< "lion count: "<< lion_count <<std::endl;
    std::cout<< "dog count: "<< dog_count <<std::endl;
    std::cout<< "cat count: "<< cat_count <<std::endl;
    std::cout<< "domesticated animals: "<< domesticated_animals <<std::endl;
    std::cout<< "narrowing conversion: "<< narrowing_conversion <<std::endl;

    return 0;
}