#include<iostream>

int main(){
    char value = 65; // ASCII character code A

    std::cout << "value: " << value << std::endl; //print value: A
    std::cout << "value(int): " << static_cast<int>(value) << std::endl;
    // value(int): 65
    return 0;
}