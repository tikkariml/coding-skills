#include <iostream>

int main(){
    //std::cout : orinting stuff to the console
    std::cout << "Hello Word!" << std::endl;

    int age {21};
    std::cout << "The age is: " << age <<std::endl;

    //Error
    std::cerr << "std::cerr output : Something went wrong" << std::endl;

    //Log message
    std::clog << "std::clog output : This a log message" << std::endl; 
}