"""
Tee ohjelma, joka kysyy käyttäjältä sanaa ja tulostaa 
sanan tähtiraameihin, joissa sana on keskellä. Raamien 
leveys on 30 merkkiä, ja voit olettaa, että sana mahtuu 
raameihin.
"""

line = "******************************"
text = str(input("Sana: "))
blankInt = ((30 - 2) / 2) - (len(text) / 2)
blank = ""
bonus = ""

while blankInt >= 1:
    blank += " "
    blankInt -= 1

if blankInt == 0.5:
    bonus += " "

print(line)
print(f"*{blank}{text}{blank}{bonus}*")
print(line)
