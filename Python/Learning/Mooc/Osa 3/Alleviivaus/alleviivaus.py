"""
Tee ohjelma, joka pyytää käyttäjältä merkkijonoja ja 
tulostaa kunkin merkkijonon oheisen esimerkin mukaisesti 
alleviivattuna. Ohjelman suoritus päättyy, kun käyttäjä 
syöttää tyhjän merkkijonon, eli merkkijonon jonka pituus on 0.
"""

while True:
    text = str(input("Anna merkkijono: "))
    if text == "":
        break
    num = len(text)

    text2 = ""
    while num != 0:
        text2 += "-"
        num -= 1
    
    print(text)
    print(text2)
