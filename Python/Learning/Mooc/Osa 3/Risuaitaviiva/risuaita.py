"""
Tee ohjelma, joka piirtää käyttäjän
määräämän levyisen risuaitaviivan.
"""

num = int(input("Leveys: "))
text = ""
while num != 0:
    text += "#"
    num -= 1
print(text)
