"""Kirjoita ohjelma, joka kysyy käyttäjältä merkkijonon ja tulostaa sitten merkkijonon merkit allekkain käänteisessä järjestyksessä lopusta alkuun."""
text = str(input("Anna merkkijono: "))
num = len(text) -1
while num != -1:
    print(text[num])
    num -=1
